<script type="text/javascript" src="ceriCarApp/js/main.js"></script>
<div class="w3-container">
  <div class="w3-card-4">
    <div class="w3-container w3-black">
      <h2>Rechercher un voyage</h2>
    </div>

    <form class="w3-container formulaire" onsubmit="return false" method="post" action="?action=getVoyage">
      <p>
      <label for="depart">Depart : </label>
      <input class="w3-input" type="text" id="depart" name="depart" required>
      <p>    
      <label for="arrivee">Arrivée : </label> 
      <input class="w3-input" type="text" id="arrivee" name="arrivee" required>
        <br>
        <label for="arrivee">Nombre de personne(s) : </label> 
        <input class="w3-input" type="number" id="nbpersonne" name="nbpersonne" value = "1" required>
        <br>
        <input type="checkbox" id="correspondance" name="correspondance" value="yes">
        <label for="correspondance">Correspondance(s)</label>
        <br>
        <br>
      <input class="w3-button w3-round w3-black" type="submit" value="Find">

    </form>
  </div>
</div>


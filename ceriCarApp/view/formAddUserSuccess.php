<script type="text/javascript" src="ceriCarApp/js/main.js"></script>
<div class="w3-container">
  <div class="w3-card-4">
    <div class="w3-container w3-green">
      <h2>Sign In</h2>
    </div>

    <form class="w3-container formulaire" onsubmit="return false" method="post" action="?action=addUser">
      <p>
      <label for="depart">Nom</label>
      <input class="w3-input" type="text" id="nom" name="nom" required>
      <p>
      <label for="arrivee">Prénom</label>
      <input class="w3-input" type="text" id="prenom" name="prenom" required>
        <br>
        <label for="arrivee">Date de Naissance</label>
      <input class="w3-input" type="text" id="birthDate" name="birthDate" placeholder="JJ/MM/AA" required>
        <br>
        <label for="arrivee">Identifiant/Pseudo</label>
      <input class="w3-input" type="text" id="identifiant" name="identifiant" required>
        <br>
        <label for="arrivee">Mot de Passe</label>
      <input class="w3-input" type="text" id="pass" name="pass" required>
        <br>
      <input class="w3-button w3-round w3-green" type="submit" value="S'inscrire">

    </form>
  </div>
</div>

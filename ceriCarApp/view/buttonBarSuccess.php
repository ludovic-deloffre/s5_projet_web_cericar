<script type="text/javascript" src="ceriCarApp/js/main.js"></script>
<div class="w3-container w3-center">
    <input class="w3-button w3-round w3-blue connection menu" align="center" type="button" action="?action=formGetUserByLoginAndPass" value="Se connecter">
    <input class="w3-button w3-round w3-green inscription menu" align="center" type="button" action="?action=formAddUser" value="S'inscrire">
    <input class="w3-button w3-round w3-blue w3-hide deconnexion" align="center" type="button" action="?action=deconnexion" value="Deconnexion">
    <input class="w3-button w3-round w3-black menu" align="center" type="button" action="?action=formGetTrajet" value="Rechercher un voyage">
    <input class="w3-button w3-round w3-hide w3-orange conducteur menu" align="center" type="button" action="?action=formAddVoyage" id="loadFormAddVoyage" value="Proposer un voyage">
</div>
var isLoading = false;

$(document).ready(function(){

    $(function(){
        
        $(".menu").click(function(e) {
        e.preventDefault();
        if(!isLoading){
            isLoading = true;
            $.ajax({
                    url: $(this).attr('action'),
                    success:function(data) { $("#page_maincontent").html(data); isLoading = false;}
                });
            }
        });
        
    });
    
    $(function(){
        $(".formulaire").submit(function(){
            if(!isLoading){
                isLoading = true;
                $.ajax({
                    url: $(this).attr('action'),
                    type: $(this).attr('method'),
                    data: $(this).serialize(),
                    success:function(data) { $("#page_maincontent").html(data); isLoading = false;}
                });
            }
                
        }); 
    });

    $(function(){
        $("#loadUser").submit(function(){
            $.ajax({
    
                url: "?action=welcomeMessage",
                success:function(data) { $("#bonjour").html(data); 
                }
            });
    
            
        });
    
    });

});

$(function(){

    $(".deconnexion").click(function(e) {

        $.ajax({
            url: $(this).attr('action'),
            success:function(data) { $("#page_maincontent").html(data); 
        }
        });
    });
});
    

$(function(){

    $(".deconnexion").click(function(e) {
  
    $(document).ready(function(){
        $.ajax({
            url: "?action=welcomeMessage",
            success:function(data){
                $('.connection').removeClass("w3-hide");
                $('.inscription').removeClass("w3-hide");
                $('.deconnexion').addClass("w3-hide");
                $('.conducteur').addClass("w3-hide");
                $("#bonjour").html(data);
            }
            
            });
    });
    
});
});


// Réservation d'un voyage
$(function(){

    $(".loadResa").click(function(e) {
    e.preventDefault();

    $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: {voyage_id:$(this).attr('data')},
            success:function(data) { $("#page_maincontent").html(data); 
        }
        });
    });
    
});
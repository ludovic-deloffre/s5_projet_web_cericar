<?php
// Inclusion de la classe utilisateur
require_once "voyage.class.php";

class voyageTable {

  public static function getVoyageByid($id)
	{
  	$em = dbconnection::getInstance()->getEntityManager() ;

	$voyageRepository = $em->getRepository('voyage');
	$voyage = $voyageRepository->findOneBy(array('id' => $id));
  //$_POST['IsError']="Recherche terminée";
	if ($voyage == false){
		//echo 'Erreur sql';
    //$_POST['IsError']="Erreur lors de la connection à la base de données";
			   }
	return $voyage;
	}

  public static function getVoyagesByTrajet($trajet, $nbpersonne){

    $em = dbconnection::getInstance()->getEntityManager() ;

    $voyageRepository = $em->getRepository('voyage');

    $voyage = $voyageRepository->findBy(array('trajet' => $trajet, 'nbplace' => $nbpersonne));

    $_POST['IsError']="Recherche terminée";
    if ($voyage == false){
      //echo 'Erreur sql';
        $_POST['IsError']="Erreur lors de la connection à la base de données";
          }
    return $voyage;

}

public static function getVoyagesByTrajetWithCorrespondance($trajet){

    $em = dbconnection::getInstance()->getEntityManager() ;
    $db = $em->getConnection();

    $query = $db->prepare("SELECT * from get_correspondanceF('".$trajet->depart."', '".$trajet->arrivee."', '".$trajet->depart."'); ");
    $query->execute();

    $query2 = $db->prepare("SELECT * from correspondance;");
    $query2->execute();
    $result = $query2->fetchAll();
    //$context->correspondance = $result;



    // $correspondanceRepository = $em->getRepository('correspondance');

    // $correspondance = $correspondanceRepository->findAll();

    //$correspondance = $correspondanceRepository->findBy(array('id' => 1));

                                                                                                                                                                           
    $_POST['IsError']="Recherche terminée";
    if ($result == false){
      //echo 'Erreur sql';
        $_POST['IsError']="Erreur lors de la connection à la base de données";
           }
    return $result;

  // findBy(array(), array('username' => 'ASC'));


  // $RAW_QUERY2 = "SELECT * FROM correspondance;";

  // $_POST['IsError']="Recherche terminée";
  // if ($RAW_QUERY2 == false){
  //   //echo 'Erreur sql';
  //     $_POST['IsError']="Erreur lors de la connection à la base de données";
  //        }
  
  // $query1 = $db->prepare($RAW_QUERY2);
  // $query1->execute();
  
  // $result = $query1->fetchAll();
  // $context->correspondance = $result;

  // //return $context->voyage;
  // return $result;

}



public static function getVoyagesByConducteur($user_id){

  $em = dbconnection::getInstance()->getEntityManager() ;

$voyageRepository = $em->getRepository('voyage');

$voyage = $voyageRepository->findBy(array('conducteur' => $user_id));
if ($voyage == false){
  //echo 'Erreur sql';
       }
return $voyage;
}


public static function decrementNbPlaceVoyage($voyage_id){

	$em = dbconnection::getInstance()->getEntityManager() ;
	$db = $em->getConnection();

	$query = $db->prepare("update jabaianb.voyage set nbplace = nbplace -1 where id = ?");
	$query->bindParam(1, $voyage_id, PDO::PARAM_INT);
  $query->execute();

  // $qb = $this->em->createQueryBuilder();
  // $q = $qb->update('jabaianb.voyage', 'u')
  //       ->set('v.nbplace = v.nbplace - 1')
  //       ->where('v.id = ?')
  //       ->setParameter(1, $voyage_id)
  //       ->getQuery();
  // $p = $q->execute();

  return $query;

}

public static function addVoyage($trajet_id, $heure, $nbplace, $tarif, $contraintes){

	$em = dbconnection::getInstance()->getEntityManager() ;
  $db = $em->getConnection();

	$query = $db->prepare("INSERT into jabaianb.voyage (conducteur, trajet, tarif, nbplace, heuredepart, contraintes) VALUES (?, ?, ?, ?, ?, ?)");
  $query->bindParam(1, $_SESSION['user_id'], PDO::PARAM_INT);
  $query->bindParam(2, $trajet_id, PDO::PARAM_INT);
  $query->bindParam(3, $tarif, PDO::PARAM_INT);
  $query->bindParam(4, $nbplace, PDO::PARAM_INT);
  $query->bindParam(5, $heure, PDO::PARAM_INT);
  $query->bindParam(6, $contraintes, PDO::PARAM_STR);
  $query->execute();

  return $query;

  // $voyage =new voyage;

  // $voyage->conducteur=$_SESSION['user_id'];
	// $voyage->trajet=$trajet_id;
  // $voyage->tarif=$tarif;
  // $voyage->nbplace=$nbplace;
  // $voyage->heuredepart=$heure;
  // $voyage->contraintes=$contraintes;
  
	// $em->persist($voyage);
  // $em->flush();
  
  //return $em;

}

}


?>

<?php
// Inclusion de la classe utilisateur
require_once "utilisateur.class.php";

use Doctrine\DBAL\Query\QueryBuilder;

class utilisateurTable {

  public static function getUserByLoginAndPass($login, $pass)
	{
  	$em = dbconnection::getInstance()->getEntityManager() ;

	$userRepository = $em->getRepository('utilisateur');
	$user = $userRepository->findOneBy(array('identifiant' => $login, 'pass' => sha1($pass)));

	if ($user == false){
		$_POST['IsError']="Mauvais identifiant/mdp";
			   }
  
	return $user;
	}

  public static function getUserById($id){
    $em = dbconnection::getInstance()->getEntityManager() ;
    $userRepository = $em->getRepository('utilisateur');
  	$user = $userRepository->findOneBy(array('id' => $id));

    if ($user == false){
  		echo 'Erreur sql';
  			   }
  	return $user;
  }

  public static function addUser($nom, $prenom, $login, $pass){

	// $em = dbconnection::getInstance()->getEntityManager() ;
	// $db = $em->getConnection();
	
	// $query = $db->prepare("INSERT into jabaianb.utilisateur (identifiant, pass, nom, prenom) VALUES (?, ?, ?, ?)");
	// $sha1Pass = sha1($pass);
	// $query->bindParam(1, $login, PDO::PARAM_STR);
	// $query->bindParam(2, $sha1Pass, PDO::PARAM_STR);
	// $query->bindParam(3, $nom, PDO::PARAM_STR);
	// $query->bindParam(4, $prenom, PDO::PARAM_STR);

	// $query->execute();

	$em = dbconnection::getInstance()->getEntityManager() ;
	$user =new utilisateur;

	$sha1Pass = sha1($pass);

	$user->identifiant=$login;
	$user->pass=$sha1Pass;
	$user->nom=$nom;
	$user->prenom=$prenom;
	$em->persist($user);
	$em->flush();

	return $em;

  }


}


?>

<?php
$cpt = false;
class mainController
{

	// Testez le “helloWorld” pour vérifier son bon fonctionnement.
	public static function helloWorld($request,$context)
	{
		$context->mavariable="hello world";
		return context::SUCCESS;
	}

	public static function index($request,$context){
		return context::SUCCESS;
	}

	public static function buttonBar($request,$context){
		if(!isset($_SESSION['isConducteur'])){
			$_SESSION['isConducteur'] = 0;
		}
		return context::SUCCESS;
	}

	public static function test($request, $context){

		$context->utilisateur = utilisateurTable::getUserByLoginAndPass('User1','monMotdePasse');
		$context->trajet = trajetTable::getTrajet('Montpellier','Bordeaux');
		$context->voyage1 = voyageTable::getVoyagesByTrajet($context->trajet);
		$context->reservation1 = reservationTable::getReservationByVoyage($context->voyage1);
		$context->utilisateurbyid = utilisateurTable::getUserById(7404);

		return context::SUCCESS;
	}

	// public static function getVoyage($request, $context){
	// 	//$context->trajet = trajetTable::getTrajet($_GET["depart"], $_GET["arrivee"]);
	// 	$context->trajet = trajetTable::getTrajet('Montpellier','Bordeaux');
	// 	$context->voyage1 = voyageTable::getVoyagesByTrajet($context->trajet);

	// 	return context::SUCCESS;
	// }

	public static function formGetTrajet($request, $context){

		return context::SUCCESS;
	}

	public static function formGetUserByLoginAndPass($request, $context){

		return context::SUCCESS;
	}

	public static function formAddUser($request, $context){

		return context::SUCCESS;
	}

	public static function formAddVoyage($request, $context){

		return context::SUCCESS;
	}

	public static function welcomeMessage($request, $context){
		return context::SUCCESS;
	}

	public static function deconnexion($request, $context){
		session_unset();
		return context::SUCCESS;
	}


	public static function getVoyage($request, $context){
		if (isset($_POST['correspondance'])){
			$_POST['isCorrespondance'] = true;
			$context->trajet2 = trajetTable::getTrajet($_POST["depart"], $_POST["arrivee"]);
			$context->voyage1 = voyageTable::getVoyagesByTrajet($context->trajet2, $_POST["nbpersonne"]);
			//$context->correspondance = voyageTable::getVoyagesByTrajetWithCorrespondance($context->trajet2);
			$context->result = voyageTable::getVoyagesByTrajetWithCorrespondance($context->trajet2);
			if($context->voyage1 != false && $context->trajet2 != false){
				return context::SUCCESS;
			}
			return context::ERROR;
		}
		else{
			$_POST['isCorrespondance'] = false;
			$context->trajet = trajetTable::getTrajet($_POST["depart"], $_POST["arrivee"]);
			$context->voyage1 = voyageTable::getVoyagesByTrajet($context->trajet, $_POST["nbpersonne"]);
			if($context->trajet != false && $context->voyage1 != false){
				return context::SUCCESS;
			}
			return context::ERROR;
		}


	}

	public static function getUserByLoginAndPass($request, $context){

		$context->user = utilisateurTable::getUserByLoginAndPass($_POST["login"], $_POST["psw"]);
		if($context->user!=false){
			echo '<script type = "text/javascript">';
			echo '$(document).ready(function(){
                    $.ajax({
                        success:function(){
							$(\'.connection\').addClass("w3-hide");
							$(\'.inscription\').addClass("w3-hide");
							$(\'.deconnexion\').removeClass("w3-hide");
                        }
                    });
                    })' ;
			echo '</script>';
			//$_SESSION['user_id']=$_POST["login"];
			$_SESSION['user_identifiant']=$context->user->nom;
			$_SESSION['user_id']=$context->user->id;
			$context->isConducteur = voyageTable::getVoyagesByConducteur($context->user->id);
			if($context->isConducteur != false){
				$_SESSION['isConducteur'] = 1;
				echo '<script type = "text/javascript">';
				echo '$(document).ready(function(){
                    $.ajax({
                        success:function(){
							$(\'.conducteur\').removeClass("w3-hide");
                        }
                    });
                    })' ;
				echo '</script>';

			}else{
				$_SESSION['isConducteur'] = 0; 
				echo '<script type = "text/javascript">';
				echo '$(document).ready(function(){
                    $.ajax({
                        success:function(){
                            $(\'.conducteur\').addClass("w3-hide");
                        }
                    });
                    })' ;
				echo '</script>';
			}

		 	return context::SUCCESS;
		}
		else{
			return context::ERROR;
		}
	}



	// Définissez une action “superTest” qui prend deux paramètres dans l’url (param1 et
	// param2 par exemple) et affichez ce message: “j’ai compris <VALEUR PARAM1>,
	// super : <VALEUR PARAM2>”
	public static function superTest($request,$context){

		$context->param1=$_GET["myParam1"];
		$context->param2=$_GET["myParam2"];

		return context::SUCCESS;
	}

	public static function addUser($request, $context){
		$context->user = utilisateurTable::addUser($_POST["nom"], $_POST["prenom"], $_POST["identifiant"], $_POST["pass"]);
		if($context->user!=false){
		 	return context::SUCCESS;
		}
		else{
			return context::ERROR;
		}

	}

	public static function addResa($request, $context){
		$voyage_id = $_GET['voyage_id'];
		$context->voyage = voyageTable::getVoyageById($voyage_id);

		if($context->voyage->nbplace <= 0){
			return context::ERROR;
		}
		else{
			// $voyage_id = json_decode($data);
			$context->reservation = reservationTable::addResa($voyage_id);
			$context->voyage2 = voyageTable::decrementNbPlaceVoyage($voyage_id);
			if($context->reservation!=false){
				return context::SUCCESS;
			}
			else{
				return context::ERROR;
			}
		}
	}

	public static function addVoyage($request, $context){


		$context->trajet = trajetTable::getTrajet($_POST["depart"], $_POST["arrivee"]);
		if($context->trajet!=false){
			$tarifFinal = $context->trajet->distance * $_POST["tarif"];
			$context->voyage = voyageTable::addVoyage($context->trajet->id, $_POST["heure"], $_POST["nbplace"], $tarifFinal, $_POST["contraintes"]);
			if($context->voyage!=false){
				return context::SUCCESS;
		   	}
		   return context::ERROR;
		}
		else{
			return context::ERROR;
		}

	}
	

	


}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
      <script type="text/javascript" src="ceriCarApp/js/main.js"></script>
    <title>
     Ceri Car
    </title>
    <div class="w3-cell-row">
      <div class="w3-container w3-light-grey w3-cell" id="title">
        <h2>Ceri Car App</h2>
      </div>
      <div class="w3-container w3-light-grey w3-cell w3-cell-middle w3-cell-right" id="bonjour">
        <!-- // affiche message de bienvenu -->
      </div> 
    </div>
  
  </head>

  <body>
<br>
  <div id="buttonBar">
          <?php include($nameApp."/view/buttonBarSuccess.php"); ?>
  </div>
  <br>
  <div class="w3-center">
    <div id="page">
      <?php if($context->error): ?>
          <div id="flash_error" class="error">
            <?php echo " $context->error !!!!!" ?>
          </div>
      <?php endif; ?>
      <div id="page_maincontent">
          <?php include($template_view); ?>
      </div>
    </div>
  </div>
  </body>

</html>

<?php
//nom de l'application
$nameApp = "ceriCarApp";

//action par défaut
$action = "index";

if(key_exists("action", $_REQUEST))
$action =  $_REQUEST['action'];

require_once 'lib/core.php';
require_once $nameApp.'/controller/mainController.php';

foreach(glob($nameApp.'/model/*.class.php') as $model)
    include_once $model ;

session_start();

$context = context::getInstance();
$context->init($nameApp);


$view=$context->executeAction($action, $_REQUEST);

//traitement des erreurs de bases, reste a traiter les erreurs d'inclusion
if($view===false)
{
    $notif = "Une grave erreur s'est produite, il est probable que l'action ".$action." n'existe pas...";
    include($nameApp."/layout/".$context->getLayout().".php");
    die;
}
//inclusion du layout qui va lui meme inclure le template view

elseif($view!=context::NONE)
{
    //echo "l'action est ". $action;
    
    if($action == 'index'){
        ///echo "nombre de vié";
        //session_unset();
        $template_view=$nameApp."/view/".$action.$view.".php";
        include($nameApp."/layout/".$context->getLayout().".php");

    }
    else{
        $notif = "Tout fonctionne, l'action ".$action." existe bien.";
        include($template_view=$nameApp."/view/".$action.$view.".php");
    }
}

?>
